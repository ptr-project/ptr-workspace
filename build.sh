#!/bin/bash -ex
mkdir -p build
pushd build
conan lock create -l ../transport-routing/conan.lock --lockfile-out conan.lock.full ../transport-routing/conanfile.py
conan workspace install -l conan.lock.full ../conanws.yml -s compiler.cppstd=17 -s transport-routing:build_type=Debug -s catch2_extended:build_type=Debug -s sqlite_utils:build_type=Debug -o boost:header_only=False -o transport-routing:build_testing=True -o catch2_extended:build_testing=True -o sqlite_utils:build_testing=True --build=outdated
popd
cmake -G Ninja -H. -Bbuild/ -DCMAKE_BUILD_TYPE=Debug -DROUTING_MAP=ON -DCMAKE_TOOLCHAIN_FILE=transport-routing/cmake/toolchains/clang.cmake
ninja -C build
pushd build
ctest --output-on-failure -j 4
popd

cmake_minimum_required(VERSION 3.0)

project(ptr-workspace)

set(CONAN_EXPORTED ON)

include(${CMAKE_BINARY_DIR}/conanworkspace.cmake)
conan_workspace_subdirectories()

OPTION(BUILD_TESTING "Build the tests." ON)
if(BUILD_TESTING)
  enable_testing()
endif()
